# [`opm`](https://gitlab.com/dune-archiso/repository/opm) repository for Arch Linux

[![pipeline status](https://gitlab.com/dune-archiso/repository/opm/badges/main/pipeline.svg)](https://gitlab.com/dune-archiso/repository/opm/-/commits/main)
[![coverage report](https://gitlab.com/dune-archiso/repository/opm/badges/main/coverage.svg)](https://gitlab.com/dune-archiso/repository/opm/-/commits/main)

This is a third-party auto-updated repository with the following [tarballs](https://gitlab.com/dune-archiso/repository/opm/-/raw/main/packages.x86_64). The OPM modules are here.

## [Usage](https://gitlab.com/dune-archiso/repository/opm/-/pipelines/latest)

### Add repository

1. Import GPG key from GPG servers.

```console
[user@hostname ~]$ sudo pacman-key --recv-keys 2403871B121BD8BB
[user@hostname ~]$ sudo pacman-key --finger 2403871B121BD8BB
[user@hostname ~]$ sudo pacman-key --lsign-key 2403871B121BD8BB
```

2. Append the following lines to `/etc/pacman.conf`:

```toml
[opm]
SigLevel = Required DatabaseOptional
Server = https://dune-archiso.gitlab.io/repository/opm/$arch
```

### List packages

To show actual packages list:

```console
[user@hostname ~]$ pacman -Sl opm
```

### Install packages

To install package:

```console
[user@hostname ~]$ sudo pacman -Syyu
[user@hostname ~]$ sudo pacman -S python-opm-simulators
```

<!-- ### `PKGBUILD`s from the [Arch User Repository](https://wiki.archlinux.org/title/Arch_User_Repository) 👀
- [`dune-common`](https://aur.archlinux.org/pkgbase/dune-common)
- [`python-dune-common`](https://aur.archlinux.org/packages/python-dune-common)
- [`dune-istl`](https://aur.archlinux.org/packages/dune-istl)
- [`python-dune-istl`](https://aur.archlinux.org/packages/python-dune-istl)
- [`dune-geometry`](https://aur.archlinux.org/packages/dune-geometry)
- [`python-dune-geometry`](https://aur.archlinux.org/packages/python-dune-geometry)
- [`dune-localfunctions`](https://aur.archlinux.org/packages/dune-localfunctions)
- [`python-dune-localfunctions`](https://aur.archlinux.org/packages/python-dune-localfunctions)
- [`dune-grid`](https://aur.archlinux.org/packages/dune-grid)
- [`python-dune-grid`](https://aur.archlinux.org/packages/python-dune-grid) -->
<!--
- [`dune-common`](https://gitlab.com/dune-archiso/pkgbuilds/dune/-/raw/main/PKGBUILDS/dune-common/README.md)
- [`dune-geometry`](https://gitlab.com/dune-archiso/pkgbuilds/dune/-/raw/main/PKGBUILDS/dune-geometry/README.md)
- [`dune-dune-istl`](https://gitlab.com/dune-archiso/pkgbuilds/dune/-/raw/main/PKGBUILDS/dune-istl/README.md)
- [`dune-localfunctions`](https://gitlab.com/dune-archiso/pkgbuilds/dune/-/raw/main/PKGBUILDS/dune-localfunctions/README.md)
- [`dune-grid`](https://gitlab.com/dune-archiso/pkgbuilds/dune/-/raw/main/PKGBUILDS/dune-grid/README.md)
-->